import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import PaymentCustomer from "./components/PaymentCustomer";
import PaymentAdd from "./components/PaymentAdd";
import PaymentSearch from "./components/PaymentSearch";

function App() {
  return (
    <div className="App">
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/home" className="navbar-brand">
          Home
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/addpayment"} className="nav-link">
              Add Payment
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/customer"} className="nav-link">
              Customer
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/search"} className="nav-link">
              Search
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/about"} className="nav-link">
              About
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/home"]} component={Home} />
          <Route exact path="/addpayment" component={PaymentAdd} />
          <Route exact path="/about" component={About} />
          <Route exact path="/customer" component={PaymentCustomer} />
          <Route exact path="/search" component={PaymentSearch} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
