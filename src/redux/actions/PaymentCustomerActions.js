import {
    PAYMENT_CUSTOMER_REQUEST,
    PAYMENT_CUSTOMER_SUCCESS,
    PAYMENT_CUSTOMER_FAIL
} from "../constants/PaymentSystemConstants";
import PaymentSystemService from "../../services/PaymentSystemService";

const searchByCustomerId = (custId) => dispatch => {
    try {
        dispatch({type: PAYMENT_CUSTOMER_REQUEST});
        PaymentSystemService.findByCustomerId(custId)
            .then(response => {
                dispatch({type: PAYMENT_CUSTOMER_SUCCESS, payload: response.data})
                console.log("Response Data >>> ", response.data)
            })
    } catch(error) {
        dispatch({type: PAYMENT_CUSTOMER_FAIL, payload: error})
    }
}

export default searchByCustomerId;