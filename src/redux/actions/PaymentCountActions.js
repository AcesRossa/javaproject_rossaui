import { PAYMENT_COUNT_REQUEST, PAYMENT_COUNT_SUCCESS, PAYMENT_COUNT_FAIL } from "../constants/PaymentSystemConstants"
import PaymentSystemService from "../../services/PaymentSystemService";

const paymentCount = () =>  (dispatch) => {
    try {
        dispatch({ type: PAYMENT_COUNT_REQUEST });
        PaymentSystemService.getCount()
            .then(response => {
                dispatch({ type: PAYMENT_COUNT_SUCCESS, payload: response.data})
            });
    } catch (error) {
        dispatch({ type: PAYMENT_COUNT_FAIL, payload: error });
    }
};

export default paymentCount;