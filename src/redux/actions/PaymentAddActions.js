import { PAYMENT_ADD_REQUEST, PAYMENT_ADD_SUCCESS, PAYMENT_ADD_FAIL } from "../constants/PaymentSystemConstants"
import PaymentSystemService from "../../services/PaymentSystemService";

const paymentAdd = (payment) =>  (dispatch) => {
    try {
        dispatch({ type: PAYMENT_ADD_REQUEST });
        PaymentSystemService.create(payment)
            .then(response => {
                dispatch({ type: PAYMENT_ADD_SUCCESS})
            });
    } catch (error) {
        dispatch({ type: PAYMENT_ADD_FAIL, payload: error });
    }
};

export default paymentAdd;