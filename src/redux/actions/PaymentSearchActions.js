import {
    PAYMENT_SEARCH_BY_ID_REQUEST, PAYMENT_SEARCH_BY_ID_SUCCESS, PAYMENT_SEARCH_BY_ID_FAIL,
    PAYMENT_SEARCH_BY_TYPE_REQUEST, PAYMENT_SEARCH_BY_TYPE_SUCCESS, PAYMENT_SEARCH_BY_TYPE_FAIL
} from "../constants/PaymentSystemConstants"
import PaymentSystemService from "../../services/PaymentSystemService";

export const searchById = (id) => (dispatch) => {
    try {
        dispatch({type: PAYMENT_SEARCH_BY_ID_REQUEST});
        PaymentSystemService.findById(id)
            .then(response => {
                dispatch({type: PAYMENT_SEARCH_BY_ID_SUCCESS, payload: response.data})
            });
    } catch (error) {
        dispatch({type: PAYMENT_SEARCH_BY_ID_FAIL, payload: error});
    }
};

export const searchByType = (type) => (dispatch) => {
    try {
        dispatch({type: PAYMENT_SEARCH_BY_TYPE_REQUEST});
        PaymentSystemService.findByType(type)
            .then(response => {
                dispatch({type: PAYMENT_SEARCH_BY_TYPE_SUCCESS, payload: response.data})
                console.log("RESPONSE DATA >>> ", response.data)
            });
    } catch (error) {
        dispatch({type: PAYMENT_SEARCH_BY_TYPE_FAIL, payload: error});
    }
};

export default {searchById, searchByType};