import {
    PAYMENT_SEARCH_BY_ID_REQUEST,
    PAYMENT_SEARCH_BY_ID_SUCCESS,
    PAYMENT_SEARCH_BY_ID_FAIL,
    PAYMENT_SEARCH_BY_TYPE_REQUEST,
    PAYMENT_SEARCH_BY_TYPE_SUCCESS,
    PAYMENT_SEARCH_BY_TYPE_FAIL
} from "../constants/PaymentSystemConstants"

const paymentSearchInitialState = {
    payment: [],
    payments: [],
    error: null,
    validId: false,
    validType: false
}

function PaymentSearchReducer(state = paymentSearchInitialState, action) {

    switch (action.type) {
        case PAYMENT_SEARCH_BY_ID_REQUEST:
            return {...state, validId: false, validType: false, payments: []};
        case PAYMENT_SEARCH_BY_ID_SUCCESS:
            return {...state, payment: action.payload, validId: true, validType: false};
        case PAYMENT_SEARCH_BY_ID_FAIL:
            return {...state, error: action.payload, validId: false, validType: false};
        case PAYMENT_SEARCH_BY_TYPE_REQUEST:
            return {...state, validId: false, validType: false, payment: null};
        case PAYMENT_SEARCH_BY_TYPE_SUCCESS:
            return {...state, payments: action.payload, validId: false, validType: true};
        case PAYMENT_SEARCH_BY_TYPE_FAIL:
            return {...state, error: action.payload, validId: false, validType: false};
        default:
            return state;
    }
}

export default PaymentSearchReducer;