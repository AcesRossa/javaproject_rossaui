import {
    PAYMENT_ADD_REQUEST,
    PAYMENT_ADD_SUCCESS,
    PAYMENT_ADD_FAIL
} from "../constants/PaymentSystemConstants"

const paymentInitialState = {
    submitted: false,
    error: null
}

function PaymentAddReducer(state = paymentInitialState, action) {
    switch (action.type) {
        case PAYMENT_ADD_REQUEST:
            return {...state};
        case PAYMENT_ADD_SUCCESS:
            return {...state, submitted: true};
        case PAYMENT_ADD_FAIL:
            return {...state, submitted: false, error: action.payload};
        default:
            return state;
    }
}

export default PaymentAddReducer;