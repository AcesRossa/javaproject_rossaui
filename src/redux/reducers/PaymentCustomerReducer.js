import {
    PAYMENT_CUSTOMER_REQUEST,
    PAYMENT_CUSTOMER_SUCCESS,
    PAYMENT_CUSTOMER_FAIL
} from "../constants/PaymentSystemConstants"

const paymentCustomerInitialState = {
    payments: [],
    error: null,
    isCustomer: false
}

function PaymentCustomerReducer(state = paymentCustomerInitialState, action) {
    switch (action.type) {
        case PAYMENT_CUSTOMER_REQUEST:
            return {...state, isCustomer: false};
        case PAYMENT_CUSTOMER_SUCCESS:
            return {...state, isCustomer: true, payments: action.payload};
        case PAYMENT_CUSTOMER_FAIL:
            return {...state, isCustomer: false, error: action.payload};
        default:
            return state;
    }
}

export default PaymentCustomerReducer;