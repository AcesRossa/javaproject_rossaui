import { combineReducers } from 'redux'
import PaymentAddReducer from "./PaymentAddReducer";
import PaymentCountReducer from "./PaymentCountReducer"
import PaymentCustomerReducer from "./PaymentCustomerReducer"
import PaymentSearchReducer from "./PaymentSearchReducer"

export default combineReducers({
    PaymentAddReducer,
    PaymentCountReducer,
    PaymentCustomerReducer,
    PaymentSearchReducer
})