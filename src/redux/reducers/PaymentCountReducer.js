import {
    PAYMENT_COUNT_REQUEST,
    PAYMENT_COUNT_SUCCESS,
    PAYMENT_COUNT_FAIL
} from "../constants/PaymentSystemConstants"

const paymentCountInitialState = {
    count: 0,
    error: null
}

function PaymentCountReducer(state = paymentCountInitialState, action) {

    switch (action.type) {
        case PAYMENT_COUNT_REQUEST:
            return {...state};
        case PAYMENT_COUNT_SUCCESS:
            return {...state, count: action.payload};
        case PAYMENT_COUNT_FAIL:
            return {...state, error: action.payload};
        default:
            return state;
    }
}

export default PaymentCountReducer;