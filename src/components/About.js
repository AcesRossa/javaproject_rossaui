import React, {Component} from 'react';
import PaymentSystemService from "../services/PaymentSystemService";

export default class About extends Component {
    constructor(props) {
        super(props);

        this.state = {
            status: "unknown"
        }
    }

    retrieveApiStatus() {
        PaymentSystemService.getStatus()
            .then(response => {
                console.log(response.data)
                this.setState({
                    status: response.data
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    componentDidMount() {
        this.retrieveApiStatus()
    }

    render() {
        return (
            <div>
                <h1>API Status: {this.state.status}</h1>
            </div>
        )
    }
}