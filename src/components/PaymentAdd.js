import React, {Component} from 'react';
import paymentAdd from "../redux/actions/PaymentAddActions";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";

class PaymentAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            paymentId: "",
            type: "",
            amount: "",
            custId: "",
            saved: false
        }

        this.makePayment = this.makePayment.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    makePayment() {
        const payment = {
            paymentId: this.state.paymentId,
            paymentDate: new Date(),
            type: this.state.type,
            amount: this.state.amount,
            custId: this.state.custId
        }
        this.props.addPayment(payment)
    }

    render() {
        if(this.props.submitted) {
            return <Redirect to="home" />
        }
        return (
            <div>
                <form className="col-6">
                    <h1>Make a Payment</h1>
                    <div className="form-group">
                        <label htmlFor="paymentId">Payment ID</label>
                        <input type="number" className="form-control" id="paymentId" aria-describedby="paymentIdHelp"
                               placeholder="Enter Payment ID" value={this.state.paymentId} name="paymentId"
                               onChange={this.onChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="paymentType">Payment Type</label>
                        <input type="test" className="form-control" id="paymentType" aria-describedby="paymentTypeHelp"
                               placeholder="Enter Payment Type" value={this.state.type} name="type"
                               onChange={this.onChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="paymentAmount">Amount</label>
                        <input type="number" className="form-control" id="paymentAmount"
                               aria-describedby="paymentAmountHelp"
                               placeholder="Enter Amount" value={this.state.amount}  name="amount"
                               onChange={this.onChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="paymentCustId">Customer ID</label>
                        <input type="number" className="form-control" id="paymentCustId"
                               aria-describedby="paymentCustIdHelp"
                               placeholder="Enter Customer ID" value={this.state.custId} name="custId"
                               onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <input type="button" className="form-control btn btn-primary" value="Make Payment"
                               onClick={this.makePayment}/>
                    </div>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addPayment: (payment) => dispatch(paymentAdd(payment))
    };
};

const mapStateToProps = (state) => {
    return {
        error: state.PaymentAddReducer.error,
        submitted: state.PaymentAddReducer.submitted
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentAdd);