import React, {Component} from 'react';
import dateFormat from 'dateformat';

export default class PaymentDetails extends Component {
    render() {
        const {paymentDetails} = this.props
        return (
            <div>
                <h4>Payment Details</h4>
                <div className="container">
                    <div className="row">
                        {paymentDetails &&
                        paymentDetails.map((payment, index) => (
                            <div className="col-6 mb-3" key={index}>
                                <div className="card">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Customer ID: {payment.custId}</li>
                                        <li className="list-group-item">Payment ID: {payment.paymentId}</li>
                                        <li className="list-group-item">Type: {payment.type}</li>
                                        <li className="list-group-item">Amount: {payment.amount}</li>
                                        <li className="list-group-item">Payment
                                            Date: {dateFormat(payment.paymentDate, "fullDate")}</li>
                                    </ul>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}