import React, {Component} from 'react';
import {searchById, searchByType} from "../redux/actions/PaymentSearchActions";
import PaymentIdView from "./PaymentIdView";
import {connect} from "react-redux";
import PaymentDetails from "./PaymentDetails";

class PaymentSearch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            findByPaymentId: "",
            findByPaymentType: ""
        }

        this.findById = this.findById.bind(this);
        this.findByType = this.findByType.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    findById() {
        this.setState({
            findByPaymentType: ""
        })
        const id = this.state.findByPaymentId
        this.props.searchById(id);
    }

    findByType() {
        this.setState({
            findByPaymentId: ""
        })
        const type = this.state.findByPaymentType
        this.props.searchByType(type);
    }

    validateIdView() {
        if(this.props.validPaymentId) {
            return(
                <PaymentIdView payment={this.props.payment}/>
            )
        } else {
            return null
        }
    }

    validateTypeView() {
        if(this.props.validPaymentType) {
            return(
                <PaymentDetails paymentDetails={this.props.payments}/>
            )
        } else {
            return null
        }
    }

    render() {
        return (
            <div>
                <h1>Search for a payment</h1>
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <h3>Find By ID</h3>
                            <div className="form-group">
                                <label htmlFor="findByPaymentId">Payment ID</label>
                                <input type="number" className="form-control" id="findByPaymentId"
                                       aria-describedby="findByPaymentIdHelp"
                                       placeholder="Enter Payment ID" value={this.state.findByPaymentId} name="findByPaymentId"
                                       onChange={this.onChange}/>
                            </div>
                            <div className="form-group">
                                <input type="button" className="form-control btn btn-primary" value="Search By ID"
                                       onClick={this.findById}/>
                            </div>
                        </div>
                        <div className="col-6">
                            <form>
                                <h3>Find By Type</h3>
                                <div className="form-group">
                                    <label htmlFor="findByPaymentType">Payment Type</label>
                                    <input type="text" className="form-control" id="findByPaymentType"
                                           aria-describedby="findByPaymentTypeHelp"
                                           placeholder="Enter Payment Type" value={this.state.findByPaymentType} name="findByPaymentType"
                                           onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <input type="button" className="form-control btn btn-primary" value="Search By Type"
                                           onClick={this.findByType}/>
                                </div>
                            </form>
                        </div>
                    </div>
                    {this.validateIdView()}
                    {this.validateTypeView()}
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        searchById: (id) => dispatch(searchById(id)),
        searchByType: (type) => dispatch(searchByType(type))
    };
};

const mapStateToProps = (state) => {
    return {
        error: state.PaymentSearchReducer.error,
        payment: state.PaymentSearchReducer.payment,
        payments: state.PaymentSearchReducer.payments,
        validPaymentId: state.PaymentSearchReducer.validId,
        validPaymentType: state.PaymentSearchReducer.validType
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentSearch);