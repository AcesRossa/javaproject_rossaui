import React from 'react';
import dateFormat from "dateformat";

function PaymentIdView(props) {
    const {payment} = props
    return (
        <div>
            <h4>Payment Details</h4>
            <div className="container">
                <div className="row">
                    <div className="col-6 mb-3">
                        <div className="card">
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item">Payment ID: {payment.custId}</li>
                                <li className="list-group-item">Payment ID: {payment.paymentId}</li>
                                <li className="list-group-item">Type: {payment.type}</li>
                                <li className="list-group-item">Amount: {payment.amount}</li>
                                <li className="list-group-item">Payment
                                    Date: {dateFormat(payment.paymentDate, "fullDate")}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PaymentIdView;