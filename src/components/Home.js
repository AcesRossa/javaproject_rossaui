import React from 'react';
import PaymentCount from "./PaymentCount";

function Home(props) {
    return (
        <div>
            <h1>Payment System Home Page</h1>
            <PaymentCount />
        </div>
    )
}

export default Home;