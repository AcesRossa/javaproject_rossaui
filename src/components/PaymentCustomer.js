import React, {Component} from 'react';
import searchByCustomerId from "../redux/actions/PaymentCustomerActions";
import PaymentDetails from "./PaymentDetails";
import {connect} from "react-redux";

class PaymentCustomer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            customerId: ""
        }

        this.onChangeCustomerId = this.onChangeCustomerId.bind(this);
        this.searchForCustomerId = this.searchForCustomerId.bind(this);
    }

    onChangeCustomerId(e) {
        this.setState({
            customerId: e.target.value
        });
    }

    searchForCustomerId() {
        const id = this.state.customerId
        this.props.searchByCustomerId(id)
    }

    validateCustomer() {
        if (this.props.isCustomer) {
            return (
                <PaymentDetails paymentDetails={this.props.payments}/>
            )
        } else {
            return null
        }
    }

    render() {
        return (
            <div>
                <form className="col-6">
                    <h1>Customer</h1>
                    <div className="form-group">
                        <label htmlFor="customerId">Customer ID</label>
                        <input type="number" className="form-control" id="customerId" aria-describedby="customerIdHelp"
                               placeholder="Enter Customer ID" value={this.state.customerId}
                               onChange={this.onChangeCustomerId}/>
                        <small id="customerHelp" className="form-text text-muted">We'll never share your details with
                            anyone else.</small>
                    </div>

                    <div className="form-group">
                        <input type="button" className="form-control btn btn-primary" value="Search"
                               onClick={this.searchForCustomerId}/>
                    </div>
                </form>
                {this.validateCustomer()}
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        searchByCustomerId: (id) => dispatch(searchByCustomerId(id))
    };
};

const mapStateToProps = (state) => {
    return {
        error: state.PaymentCustomerReducer.error,
        payments: state.PaymentCustomerReducer.payments,
        isCustomer: state.PaymentCustomerReducer.isCustomer
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentCustomer);