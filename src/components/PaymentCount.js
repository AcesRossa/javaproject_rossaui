import React, {Component} from 'react';
import paymentCount from "../redux/actions/PaymentCountActions";
import {connect} from "react-redux";

class PaymentCount extends Component {
    constructor(props) {
        super(props);
        this.props.paymentCount()
    }

    render() {
        const {count} = this.props
        return (
            <div>
                <h3>Total number of payments on system: {count}</h3>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        paymentCount: () => dispatch(paymentCount())
    };
};

const mapStateToProps = state => ({
    error: state.PaymentCountReducer.error,
    count: state.PaymentCountReducer.count
})

export default connect(mapStateToProps,mapDispatchToProps)(PaymentCount);