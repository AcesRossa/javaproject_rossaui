import http from '../http-common';

export class PaymentSystemService {
    create(data) {
        return http.post("/save", data)
    }

    getStatus() {
        return http.get("/status")
    }

    getCount() {
        return http.get("/count")
    }

    findByCustomerId(custId) {
        return http.get(`/findByCustId/${custId}`)
    }

    findById(id) {
        return http.get(`/findById/${id}`)
    }

    findByType(type) {
        return http.get(`/findByType/${type}`)
    }
}

export default new PaymentSystemService();